#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 19:53:03 2020

@author: bokab
"""

#CATATAN : Data pada file input tidak perlu dirubah atau dimanipulasi

import csv
from openpyxl import Workbook
from openpyxl.chart import BarChart, Reference

class excel:
    fileTarget = "Chandra-Aldiwijaya.xlsx"
    input1 = ""
    input2 = ""
    result = {}
    
    def __init__(self, data1, data2):
        self.input1 = data1
        self.input2 = data2
    
    def run(self):
        self.analyze()
        self.generate()
    
    def analyze(self):
        luas = {}
        with open(self.input1) as file1: 
            reader1 = csv.DictReader(file1)
            for baris in reader1:
                key = list(baris.keys())
                kecamatan = baris[key[0]]
                __luas = baris[key[1]]
                luas[kecamatan] = __luas
        
        error = []
        with open(self.input2) as file2: 
            reader2 = csv.DictReader(file2)
            for baris in reader2:
                key = list(baris.keys())
                kecamatan = baris[key[0]]
                __populasi = baris[key[2]]
                try:
                    density = (int(__populasi)/int(luas[kecamatan]))*100
                    self.result[kecamatan] = density
                
                except:
                    try:
                        import difflib
                        
                        cek = difflib.get_close_matches(kecamatan, luas)
                        if (len(cek) == 1):
                            density = (int(__populasi)/int(luas[cek[0]]))*100
                            self.result[kecamatan] = density
                        
                        else:
                            error.append(kecamatan)
                    
                    except:
                        error.append(kecamatan)
        
        
        
        if (len(error) != 0):
            return print("Warning! Error occured on " + str(error))
        
        return print("Analyze Succesful!")
    
    def generate(self):
        wb = Workbook()
        ws = wb.active
        
        n = len(self.result)
        ws.append(["Kecamatan", "Kepadatan Penduduk"])
        for key in self.result:
            baris = [key, self.result[key]]
            ws.append(baris)
        
       
        data = Reference(ws, min_col=2, min_row=1, max_row=n+1, max_col=2)
        titles = Reference(ws, min_col=1, min_row=2, max_row=n+1)
        chart = BarChart()
        chart.title = "Tingkat Kepadatan Penduduk"
        chart.type = "col"
        chart.style = 10
        chart.height = 10
        chart.width = 30
        chart.add_data(data=data, titles_from_data=True)
        chart.set_categories(titles)
        chart.y_axis.title = 'Kepadatan'
        chart.x_axis.title = 'Kecamatan'
        
        ws.add_chart(chart, "E5")
        
        wb.save(self.fileTarget)
        
        print("Excel file generated successful!")


def start():
    data1 = "luas-wilayah-menurut-kecamatan-di-kota-bandung-2017.csv"
    data2 = "jumlah-penduduk-kota-bandung.csv"
    f = excel(data1, data2)
    f.run()
    

if __name__ == "__main__":
    start()