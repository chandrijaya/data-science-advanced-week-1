#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 29 19:30:25 2020

@author: bokab
"""

import numpy as np
import pandas as pd

class program:
    data = []
    file = ""
    
    def __init__(self, filename):
        self.file = filename
    
    def analyze(self):
        tempGdp = []
        tempHdi = []
        tempProvinsi = []

        df = pd.read_csv(self.file)
        df = df.replace(np.nan, 0)
        cols = [col for col in df.columns if col in ['Country Name','Indicator Name', '2010']]
        df2 = df[cols]
        df2 = df2.loc[(df2['Indicator Name'] == 'Human Development Index') | (df2['Indicator Name'] == 'Total GDP excluding Oil and Gas (in IDR Million), Current Price')]
        df2 = df2.replace('Human Development Index', 'HDI')
        df2 = df2.replace('Total GDP excluding Oil and Gas (in IDR Million), Current Price', 'GDP')
        df2 = df2.rename(columns={'Country Name': 'Nama Provinsi'})
        for index, row in df2.iterrows():
            if row['Nama Provinsi'] not in tempProvinsi:
                tempProvinsi.append(row['Nama Provinsi'])
                
            if row['Indicator Name'] == 'HDI':
                tempHdi.append(float(row['2010']))
            
            if row['Indicator Name'] == 'GDP':
                tempGdp.append(float(row['2010']))
        
        d = {'Nama Provinsi':tempProvinsi,
                'GDP':tempGdp,
                'HDI':tempHdi
                }
        df3 = pd.DataFrame(data=d)
        return df3




def start():
    def fungsi(df):
        from openpyxl import Workbook
        from openpyxl.chart import BarChart, Reference
        
        Norm = (df.GDP - df.GDP.min())/(df.GDP.max() - df.GDP.min())
        df2 = df.copy()
        df2['GDP'] = Norm
        
        wb = Workbook()
        ws = wb.active
        
        n = len(df)
        
        ws.append(['Nama Provinsi', 'GDP', 'HDI'])
        for index, row in df2.iterrows():
            baris = [row['Nama Provinsi'], row['GDP'], row['HDI']]
            ws.append(baris)
        
        titles = Reference(ws, min_col=1, min_row=2, max_row=n+1)
        data1 = Reference(ws, min_col=2, min_row=1, max_row=n+1, max_col=2)
        data2 = Reference(ws, min_col=3, min_row=1, max_row=n+1, max_col=3)
        
        chart1 = BarChart()
        chart1.title = "GDP Provinsi"
        chart1.type = "col"
        chart1.style = 10
        chart1.height = 10
        chart1.width = 30
        chart1.add_data(data=data1, titles_from_data=True)
        chart1.set_categories(titles)
        chart1.y_axis.title = 'Total GDP'
        chart1.x_axis.title = 'Provinsi'
        
        chart2 = BarChart()
        chart2.title = "HDI Provinsi"
        chart2.type = "col"
        chart2.style = 10
        chart2.height = 10
        chart2.width = 30
        chart2.add_data(data=data2, titles_from_data=True)
        chart2.set_categories(titles)
        chart2.y_axis.title = 'HDI'
        chart2.x_axis.title = 'Provinsi'
        
        ws.add_chart(chart1, "E1")
        ws.add_chart(chart2, "E23")
        wb.save("chandra.aldiwijaya.694.xlsx")
     
    fileName = "raw_data.csv"
    f = program(fileName)
    data = f.analyze()
    fungsi(data)

if __name__ == "__main__":
    start()