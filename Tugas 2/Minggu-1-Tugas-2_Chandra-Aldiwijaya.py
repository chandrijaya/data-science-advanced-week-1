#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 18:45:10 2020

@author: bokab
"""

import json, csv
import pandas as pd


class tugas_json:
    csvFile = ""; jsonFile = ""; toJson = "";
    
    def __init__(self, csvPath="", jsonPath=""):
        if csvPath is not (""):
            self.csvFile = csvPath
            self.toJson = (csvPath.split(".")[0] + ".json")
            print("Input CSV file")
            
        if jsonPath is not (""):
            self.jsonFile = jsonPath
            print("Input JSON file")
        
    def csv_to_json(self):
        if self.csvFile is (""):
            return print("Error! No input CSV file")
        
        temp = []
        with open(self.csvFile) as csv_file: 
            csv_reader = csv.DictReader(csv_file)
            for baris in csv_reader:
                __dict = {
                        "Provinsi" : baris["Provini"],
                        "Detail" : {
                                "Pulau Bernama" : baris["Pulau Bernama"],
                                "Pulau Tak Bernama" : baris["Pulau Tak Bernama"],
                                "Total" : baris["Total"]
                                }
                        }

                temp.append(__dict)
        
        with open(self.toJson, 'w') as json_file:
            json_file.write(json.dumps(temp, indent=4))
        
        print("CSV file converted successfully!")
        

    def json_to_pandas(self):
        if self.jsonFile is (""):
            return print("Error! No input JSON file")
        
        f = pd.read_json(self.jsonFile)
        print(f)

        
def start():
    __csv = "pulau_indonesia.csv"
    __json = "country_full.json"
    cek = tugas_json(csvPath=__csv, jsonPath=__json)
    cek.csv_to_json()
    cek.json_to_pandas()


if __name__ == "__main__":
    start()